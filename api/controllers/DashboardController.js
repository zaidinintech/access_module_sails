/**
 * DashboardController
 *
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index: function (request, response) {
		Dashboard.find().exec(function (error, dashboards) {
			if (error) {
				return response.ok({message: 'Some error Occured!'});
			}

			return response.ok(dashboards);
		});
	}
};

