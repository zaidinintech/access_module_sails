/**
 * GroupController
 *
 * @description :: Server-side logic for managing groups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var GroupController = module.exports = {


	index: function (request, response) {
		Group.find().exec(function (error, groups) {
			if (error) {
				return response.ok({message: 'Some error Occured!'});
			}

			return response.ok(groups);
		});
	},

	update: function (request, response) {
		var groupParameters = GroupController.groupParameters(request);
		groupParameters['dashboards'] = GroupController.dashboardIds(request)
			.map(function(dashboardId){ return {id: dashboardId} });

		Group.update(request.param('id'), groupParameters).exec(function (error, group) {
			if (error) {
				return response.serverError({message: 'Some error Occured!'});
			}

			if (!group) {
				return response.notFound();
			}

			return response.created(group[0]);
		});
	},

	groupParameters: function (request) {
		return _.pick(request.body, _.keys(Group.attributes));
	},

	dashboardIds: function (request) {
		return request.body.dashboardIds;
	}

};

