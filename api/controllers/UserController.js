/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var UserController = module.exports = {

	index: function (request, response) {
		User.find().exec(function (error, users) {
			if (error) {
				return response.ok({message: 'Some error Occured!'});
			}

			return response.ok(users);
		});
	},

	create: function (request, response) {
		User.create(UserController.userParameters(request)).exec(function (error, user) {
			if (error) {
				return response.serverError({message: 'Some error Occured!'});
			}

			user.groups.add(UserController.groupIds(request));
			user.save(function(err) {});

			return response.created(user);
		});
	},

	show: function (request, response) {
		User.findOne({id: request.param('id')}).exec(function (error, user) {
			if (error) {
				return response.serverError({message: 'Some error Occured!'});
			}

			if (!user) {
				return response.notFound();
			}

			return response.ok(user);
		});
	},

	update: function (request, response) {
		var userParameters = UserController.userParameters(request);
		userParameters['groups'] = UserController.groupIds(request)
			.map(function(groupId){ return {id: groupId} });

		User.update(request.param('id'), userParameters).exec(function (error, user) {
			if (error) {
				return response.serverError({message: 'Some error Occured!'});
			}

			if (!user) {
				return response.notFound();
			}

			return response.created(user[0]);
		});
	},

	destroy: function (request, response) {
		User.destroy(request.param('id')).exec(function (error, user) {
			if (error) {
				return response.serverError({message: 'Some error Occured!'});
			}

			return response.ok(user[0]);
		});
	},

	userParameters: function (request) {
		return _.pick(request.body, _.keys(User.attributes));
	},

	groupIds: function (request) {
		return request.body.groupIds;
	}


};

