# Access Management Server

Rest APIs for Access Management Module.

## Requirements

	* Node `6.10.3`
	* Yarn `latest`

## Development server

Run `sails lift` for a dev server.
